package github.com.wottrich.linkedinfirebaseauth;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    private static final String GIT = "github.com/Wottrich";

    private EditText etEmail;
    private EditText etPass;
    private Button btnLogin;
    private Button btnRegister;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etEmail = (EditText) findViewById(R.id.et_email);
        etPass = (EditText) findViewById(R.id.et_pass);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnRegister = (Button) findViewById(R.id.btn_register);

        firebaseAuth = FirebaseAuth.getInstance();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!etEmail.getText().toString().isEmpty() &&
                        !etPass.getText().toString().isEmpty()){

                    firebaseAuth.createUserWithEmailAndPassword(etEmail.getText().toString(),
                                                                etPass.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    Log.d(GIT, "OnComplete" + task.isSuccessful());

                                    if (!task.isSuccessful()){
                                        Log.e(GIT, "Error Login" + task.getException());
                                        Toast.makeText(getApplicationContext(),
                                                "Erro ao registrar",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                    else {
                                        Toast.makeText(getApplicationContext(),
                                                "Usuario criado com Sucesso",
                                                Toast.LENGTH_SHORT).show();
                                    }


                                }
                            });


                }
                else{
                    Toast.makeText(
                            getApplicationContext(),
                            "Por favor preencha os campos!",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etEmail.getText().toString().isEmpty() &&
                        !etPass.getText().toString().isEmpty()){

                    firebaseAuth.signInWithEmailAndPassword(etEmail.getText().toString(),
                                                            etPass.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    Log.d(GIT, "onComplete"+task.isSuccessful());
                                    if (!task.isSuccessful()){
                                        Log.e(GIT, "Error: "+task.getException());
                                        Toast.makeText(getApplicationContext(),
                                                "Erro ao logar, usuario pode não existir",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                    else {
                                        Toast.makeText(getApplicationContext(),
                                                "Bem-vindo",
                                                Toast.LENGTH_SHORT).show();
                                        Intent it = new Intent(LoginActivity.this,
                                                                HomeActivity.class);
                                        startActivity(it);
                                    }
                                }
                            });

                }
                else{
                    Toast.makeText(
                            getApplicationContext(),
                            "Por favor preencha os campos!",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}

package github.com.wottrich.desafioappextrato.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Note Lenovo on 22/09/2017.
 */

public class Limits implements Serializable {


    @SerializedName("total_due")
    @Expose
    private String total_due;

    @SerializedName("total")
    @Expose
    private String total;

    @SerializedName("expent")
    @Expose
    private String expent;

    @SerializedName("available")
    @Expose
    private String available;

    public Limits() {
    }

    public Limits(String total_due, String total, String expent, String available) {
        this.total_due = total_due;
        this.total = total;
        this.expent = expent;
        this.available = available;
    }

    public String getTotal_due() {
        return total_due;
    }

    public void setTotal_due(String total_due) {
        this.total_due = total_due;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getExpent() {
        return expent;
    }

    public void setExpent(String expent) {
        this.expent = expent;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }


}

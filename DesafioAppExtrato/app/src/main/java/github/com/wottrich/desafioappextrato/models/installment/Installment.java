package github.com.wottrich.desafioappextrato.models.installment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Note Lenovo on 21/09/2017.
 */

public class Installment implements Serializable {

    @SerializedName("past_due")
    @Expose
    private String past_due;

    @SerializedName("carnet")
    @Expose
    private String carnet;

    @SerializedName("installment")
    @Expose
    private String installment;

    @SerializedName("value")
    @Expose
    private String value;

    @SerializedName("detail")
    @Expose
    private Detail detail;

    //fazer detail


    public Installment() { }

    public Installment(String past_due, String carnet, String installment, String value,
                       Detail detail) {
        this.past_due = past_due;
        this.carnet = carnet;
        this.installment = installment;
        this.value = value;
        this.detail = detail;
    }

    public String getPast_due() {
        return past_due;
    }

    public void setPast_due(String past_due) {
        this.past_due = past_due;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "Installment{" +
                "past_due='" + past_due + '\'' +
                ", carnet='" + carnet + '\'' +
                ", installment='" + installment + '\'' +
                ", value='" + value + '\'' +
                ", detail='" + detail + '\'' +
                '}';
    }
}

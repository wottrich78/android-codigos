package github.com.wottrich.desafioappextrato.Util;

/**
 * Created by Note Lenovo on 22/09/2017.
 */

public class Validacao {


    public static boolean testarCodigo(String codigo){
        return codigo.matches("^[A-z0-9.]{3,10}$");
    }

    public static boolean confirmarCodigo(String codigo, String confirmarCodigo){
        return codigo.equals(confirmarCodigo);
    }
}

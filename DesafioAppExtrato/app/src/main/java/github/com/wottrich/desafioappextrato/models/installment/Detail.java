package github.com.wottrich.desafioappextrato.models.installment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Note Lenovo on 21/09/2017.
 */

public class Detail implements Serializable {

    @SerializedName("overdue_days")
    @Expose
    private String overdue_days;

    @SerializedName("overdue_date")
    @Expose
    private String overdue_date;

    @SerializedName("original_value")
    @Expose
    private String original_value;

    @SerializedName("value_diff")
    @Expose
    private String value_diff;

    @SerializedName("total_value")
    @Expose
    private String total_value;

    @SerializedName("store")
    @Expose
    private String store;


    public Detail() {  }

    public Detail(String overdue_days, String overdue_date, String original_value,
                  String value_diff, String total_value, String store) {
        this.overdue_days = overdue_days;
        this.overdue_date = overdue_date;
        this.original_value = original_value;
        this.value_diff = value_diff;
        this.total_value = total_value;
        this.store = store;
    }

    public String getOverdue_days() {
        return overdue_days;
    }

    public void setOverdue_days(String overdue_days) {
        this.overdue_days = overdue_days;
    }

    public String getOverdue_date() {
        return overdue_date;
    }

    public void setOverdue_date(String overdue_date) {
        this.overdue_date = overdue_date;
    }

    public String getOriginal_value() {
        return original_value;
    }

    public void setOriginal_value(String original_value) {
        this.original_value = original_value;
    }

    public String getValue_diff() {
        return value_diff;
    }

    public void setValue_diff(String value_diff) {
        this.value_diff = value_diff;
    }

    public String getTotal_value() {
        return total_value;
    }

    public void setTotal_value(String total_value) {
        this.total_value = total_value;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    @Override
    public String toString() {
        return "Dias Atrasados: "+overdue_days
                +"\nData de Vencimento: "+overdue_date
                +"\nValor total: "+ total_value
                +"\nLoja: "+ store;
    }
}

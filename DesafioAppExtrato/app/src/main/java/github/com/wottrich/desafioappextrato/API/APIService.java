package github.com.wottrich.desafioappextrato.API;

import github.com.wottrich.desafioappextrato.models.User.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Note Lenovo on 19/09/2017.
 */

public interface APIService {

    String BASE_URL = "http://www.icoded.com.br/api/";

    @GET( "extract.php" )
    Call< User > getUser(@Query( "pwd" ) String pwd);




}
